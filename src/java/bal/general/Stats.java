package bal.general;


import entites.Contrat;
import javax.inject.Inject;


public class Stats {
  
    @Inject dao.consultation.contrat.DaoContrat  daoCont;
    
    public Float totalMontantContrats(){
    
      Float resultat= 0F;
    
      for (Contrat cont : daoCont.getTousLesContrats()){
      
           resultat+=cont.getMontantContrat();
      }
    
      return resultat;
    }
}
