
package web.controleurs;

import dao.consultation.client.DaoClient;
import entites.Client;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped

public class C101_ContConsClient {
  
    @Inject private DaoClient dao;
    
    private Long       numero;
  
    private Client    client;
    
   
    public void ecouteurRecherche(){
    
        client = dao.getLeClient(numero);
        
    }

    public void initSession(){
     FacesContext.getCurrentInstance().getExternalContext().getSession( true );
    }

    public DaoClient getDao() {
        return dao;
    }

    public void setDao(DaoClient dao) {
        this.dao = dao;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

   
}