package web.controleurs;

import entites.Technicien;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@RequestScoped

public class C101_ContConsTech {
    
    private Long    numtech;
  
    private Technicien technicien;
    
    public void ecouteurRecherche(){
        technicien= Technicien.getLeTechnicien(numtech);    
    }

    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public Long getNumtech() {
        return numtech;
    }

    public void setNumtech(Long numtech) {
        this.numtech = numtech;
    }
    
    public void initSession(){
     FacesContext.getCurrentInstance().getExternalContext().getSession( true );
    }
    
    public Technicien getTechnicien() {
        return technicien;
    }

    public void setTechnicien(Technicien technicien) {
        this.technicien = technicien;
    }
    
    //</editor-fold>
      
}

