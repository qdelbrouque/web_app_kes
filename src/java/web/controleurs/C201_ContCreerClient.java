
package web.controleurs;

import entites.Client;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

@ManagedBean
@ViewScoped
public class C201_ContCreerClient implements Serializable {

    @Inject dao.consultation.client.DaoClient daoclient;
    @Inject dao.maj.DaoMaj                    daoMaj;
    
    private Long    numcli;
    private Client client= new Client();
    private String message = "Client créé";
    private boolean fait=false;
   
    
    public void traiter(){
        
        fait=false;
        daoMaj.enregistrerEntite(client);
        fait=true;
    
    }

    public void raz(){
    
        numcli  = null;
        client  = new Client();
        fait    = false;
    }

    public Long getNumcli() {
        return numcli;
    }

    public void setNumcli(Long numcli) {
        this.numcli = numcli;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public boolean isFait() {
        return fait;
    }

    public void setFait(boolean fait) {
        this.fait = fait;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
 

    
    
}
