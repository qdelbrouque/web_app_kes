
package dto;

public class DtoTechnicien {
 
   private Long   numtech;
   private String nom;
   
   private String dateEmbauche;
   private int    anciennete;
   private String libelleGrade;
   private Float  coutHoraire;     
   
   
   //<editor-fold defaultstate="collapsed" desc="comment">
   public Long getNumtech() {
       return numtech;
   }
   
   public void setNumtech(Long numtech) {
       this.numtech = numtech;
   }
   
   public String getNom() {
       return nom;
   }
   
   public void setNom(String nom) {
       this.nom = nom;
   }
   //</editor-fold>

    public String getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(String dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    public int getAnciennete() {
        return anciennete;
    }

    public void setAnciennete(int anciennete) {
        this.anciennete = anciennete;
    }

    public String getLibelleGrade() {
        return libelleGrade;
    }

    public void setLibelleGrade(String libelleGrade) {
        this.libelleGrade = libelleGrade;
    }

    public Float getCoutHoraire() {
        return coutHoraire;
    }

    public void setCoutHoraire(Float coutHoraire) {
        this.coutHoraire = coutHoraire;
    }
    
}
