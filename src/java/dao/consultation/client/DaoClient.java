package dao.consultation.client;

import entites.Client;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class DaoClient {
    
 @PersistenceContext private EntityManager em;
 
 public Client getLeClient(Long numcli){
 
     return em.find(Client.class, numcli);
 }

 public List<Client> getTousLesClients(){
     
      return em.createQuery("Select c from Client c").getResultList();
      
    }
    
}

